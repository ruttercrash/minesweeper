#!/usr/bin/env python


"""Classic Minesweeper Game"""


from dataclasses import dataclass
from enum import Enum
import sys

import pygame as pg

from minesweeper import Minesweeper, Field


class Button(Enum):
    LEFT = 1
    RIGHT = 3


@dataclass
class GridObject:
    col: int
    row: int


@dataclass
class Click(GridObject):
    button: Button


@dataclass
class Tile(GridObject):
    image: pg.Surface

    @property
    def screen_position(self) -> tuple[int, int]:
        return (self.col+1)*21, (self.row+1)*21

    def was_clicked(self, click_position):
        if self.image.get_rect(
                topleft=self.screen_position).collidepoint(*click_position):
            return True


class Gui:
    """Pygame frontend"""
    def __init__(self, height, width):
        pg.init()
        self.screen = pg.display.set_mode((800, 400))
        self.gameboard = [
            [Tile(col, row, pg.image.load('images/covered.png').convert())
             for col in range(width)]
            for row in range(height)
        ]
        self.draw_gameboard()
        pg.display.flip()

    def draw_gameboard(self):
        """Blit tiles into Pygame surface"""
        for row in self.gameboard:
            for tile in row:
                self.screen.blit(tile.image, tile.screen_position)

    def event_loop(self):
        """Update game state according to mouse events"""
        game_state = GameState()
        while True:
            event = pg.event.poll()

            if event.type == pg.QUIT:
                break
            elif (event.type == pg.MOUSEBUTTONDOWN and
                    event.button in (Button.LEFT.value,
                                     Button.RIGHT.value)):
                try:
                    clicked_tile = next(
                       tile for row in self.gameboard
                       for tile in row
                       if tile.was_clicked(event.pos)
                    )
                except StopIteration:
                    continue
                click = Click(clicked_tile.col, clicked_tile.row, Button(event.button))
                game_state = update_model(click)
                self.update_view(game_state, click)

            pg.display.update()
            if game_state.game_over:
                pass
        pg.quit()
        sys.exit()

    def update_view(self, game_state, click):
        """Update UI according to game state changes"""
        print(f'{click}\n{game_state}')
        if game_state.game_over:
            self.change_tile_image(click.row, click.col, 'exploded.png')
        if game_state.toggle_flag:
            if game_state.toggle_flag.has_flag:
                self.change_tile_image(click.row, click.col, 'flag.png')
            else:
                self.change_tile_image(click.row, click.col, 'covered.png')

        if click.button == Button.LEFT:
            for field in game_state.open_fields:
                if field.adjacent_bombs:
                    self.change_tile_image(field.row, field.col, f'{field.adjacent_bombs}.png')
                else:
                    self.change_tile_image(field.row, field.col, 'opened.png')

    def change_tile_image(self, row, col, imagefile):
        tile = self.gameboard[row][col]
        tile.image = pg.image.load(f'images/{imagefile}')
        self.screen.blit(tile.image, tile.screen_position)


@dataclass
class GameState:
    game_over: bool = False
    open_fields: tuple[Field] = tuple()
    toggle_flag: Field = None


def update_model(click: Click) -> GameState:
    """ Controller callback used by event loop
    """
    field = game.fields[click.row][click.col]
    if click.button == Button.LEFT:
        print("Field ", field)
        game.open(field)
        if game.result:
            return GameState(game_over=True)

        return GameState(open_fields=(
            tuple(field for field in game.flat_fields
                  if field.is_open)
        ))
    if click.button == Button.RIGHT:
        if not field.is_open:
            field.has_flag = not field.has_flag  # toggle flag
            return GameState(toggle_flag=field)


if __name__ == '__main__':
    game = Minesweeper(height=10, width=20, bombs=20)
    gui = Gui(game.height, game.width)
    gui.event_loop()
