#!/usr/bin/env python


from dataclasses import dataclass
from enum import Enum
from functools import reduce
import random
import sys


class Result(Enum):
    WON = 'won'
    LOST = 'lost'


@dataclass
class Field:
    col: int
    row: int

    is_open = False
    has_flag = False
    has_bomb = False
    adjacent_bombs = None

    def __repr__(self):
        return ''.join((
            f'x={self.col} ',
            f'y={self.row}',
            '⚑' if self.has_flag else ' ',
            '💣' if self.has_bomb else ' ',
            str(self.adjacent_bombs) if self.adjacent_bombs else ' ',
        ))

    def is_neighbour(self, other):
        return (abs(self.col - other.col) in range(2) and
                abs(self.row - other.row) in range(2) and
                self is not other)


class Minesweeper:
    def __init__(self, height=10, width=10, bombs=10):
        self.height = height
        self.width = width
        self.bombs_total = bombs
        self.result = None

        self.fields = [
            [Field(x, y) for x in range(width)]
            for y in range(height)
        ]
        self.shuffle_bombs()
        self.store_number_of_adjacent_bombs_in_fields()

    def __repr__(self):
        return '\n'.join([str(row) for row in self.fields])

    @property
    def flat_fields(self) -> list[Field]:
        return [field for row in self.fields
                for field in row]

    def shuffle_bombs(self):
        for field in random.sample(self.flat_fields, self.bombs_total):
            field.has_bomb = True

    def neighbours_iter(self, field) -> iter:
        return iter(other for other in self.flat_fields
                    if field.is_neighbour(other))

    def all_neighbours_open(self, field) -> bool:
        open_neighbours = (
            True if neighbour.is_open else False
            for neighbour in self.neighbours_iter(field)
             )
        return reduce(lambda a, b: a and b, open_neighbours)

    def calc_adjacent_bombs(self, field) -> int:
        return sum(1 for neighbour in self.neighbours_iter(field)
                   if neighbour.has_bomb)

    def store_number_of_adjacent_bombs_in_fields(self):
        for row in self.fields:
            for field in row:
                field.adjacent_bombs = self.calc_adjacent_bombs(field)

    def open(self, field):
        if field.has_flag or field.is_open:
            return
        if field.has_bomb:
            self.result = Result.WON
            return
        self.recursively_open_neighbours_without_adjacent_bombs(field)

    def recursively_open_neighbours_without_adjacent_bombs(self, field):
        field.is_open = True
        if field.adjacent_bombs:
            return
        try:
            while True:
                next_neighbour = next(
                    neighbour for neighbour in self.neighbours_iter(field)
                    if not neighbour.is_open)
                self.recursively_open_neighbours_without_adjacent_bombs(next_neighbour)
        except StopIteration:
            return


if __name__ == '__main__':
    ms = Minesweeper()
